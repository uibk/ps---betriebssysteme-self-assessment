#include "btree.h"
#include <assert.h>

btree *btree_create()
{
    btree *binaryTree = malloc(sizeof(btree));
    if (binaryTree == NULL)
    {
        fprintf(stderr, "out of memory!");
        return NULL;
    }
    else
    {
        binaryTree->rootNode = NULL;
        binaryTree->numberOfNodes = 0;
        return binaryTree;
    }
}

btree_node *node_create(int value)
{
    btree_node *newNode = malloc(sizeof(btree_node));
    if (newNode == NULL)
    {
        fprintf(stderr, "out of memory!");
        return NULL;
    }
    else
    {
        newNode->value = value;
        newNode->leftChildNode = NULL;
        newNode->rightChildNode = NULL;
    }
    return newNode;
}

void btree_destroy(btree *t)
{
    btree_node *currentNode = t->rootNode, *tmp;
    while (currentNode != NULL)
    {
        tmp = currentNode;
        if (currentNode->leftChildNode == NULL && currentNode->rightChildNode == NULL)
        {
            free(currentNode);
            currentNode = NULL;
        }
        else
        {
            currentNode = currentNode->leftChildNode;
        }
    }
}

void btree_insert(btree *t, int d)
{
    btree_node *currentNode;
    btree_node *newNode;

    // If there isn't a root node existing, create new one
    if (t->rootNode == NULL)
    {
        t->rootNode = node_create(d);
        t->numberOfNodes++;
        return;
    }
    else
    {
        currentNode = t->rootNode;
        newNode = node_create(d);
    }

    while (currentNode != NULL)
    {
        if (d > currentNode->value)
        {
            if (currentNode->rightChildNode != NULL)
            {
                currentNode = currentNode->rightChildNode;
            }
            else
            {
                currentNode->rightChildNode = newNode;
                t->numberOfNodes++;
            }
        }
        else
        {
            if (currentNode->leftChildNode != NULL)
            {
                currentNode = currentNode->leftChildNode;
            }
            else
            {
                currentNode->leftChildNode = newNode;
                t->numberOfNodes++;
            }
        }
    }
}

void btree_remove(btree *t, int d)
{
    btree_node *currentNode = t->rootNode, *tmp;

    while (currentNode != NULL)
    {
        tmp = currentNode;
        if (d > currentNode->value)
        {
            if (currentNode->rightChildNode != NULL)
            {
                currentNode = currentNode->rightChildNode;
            }
            else
            {
                currentNode->value = 0;
                tmp->rightChildNode = NULL;
                t->numberOfNodes--;
            }
        }
        else
        {
            if (currentNode->leftChildNode != NULL)
            {
                currentNode = currentNode->leftChildNode;
            }
            else
            {
                currentNode->value = 0;
                tmp->leftChildNode = NULL;
                t->numberOfNodes--;
            }
        }
    }
}

int btree_minimum(const btree *t)
{
    btree_node *currentNode = t->rootNode;
    int currentValue = currentNode->value;

    while (currentNode->leftChildNode != NULL)
    {
        currentNode = currentNode->leftChildNode;
        currentValue = currentNode->value;
    }
    return currentValue;
}

int btree_maximum(const btree *t)
{
    btree_node *currentNode = t->rootNode;
    int currentValue = currentNode->value;

    while (currentNode->rightChildNode != NULL)
    {
        currentNode = currentNode->rightChildNode;
        currentValue = currentNode->value;
    }
    return currentValue;
}

bool btree_contains(const btree *t, int d)
{
    btree_node *currentNode = t->rootNode;

    while (currentNode != NULL)
    {
        if (d == currentNode->value)
        {
            return true;
        }

        if (d > currentNode->value)
        {
            currentNode = currentNode->rightChildNode;
        }
        else
        {
            currentNode = currentNode->leftChildNode;
        }
    }
    return false;
}

size_t btree_size(const btree *t)
{
    return t->numberOfNodes;
}

void btree_print(const btree *t, FILE *out)
{
    fprintf(out, "%zu", t->numberOfNodes);
}

int main()
{
    btree *ta = btree_create();

    assert(btree_size(ta) == 0);
    assert(btree_contains(ta, 4) == false);
    btree_print(ta, stdout);

    btree_insert(ta, 4);
    btree_insert(ta, 7);
    btree_insert(ta, 3);

    assert(btree_size(ta) == 3);
    assert(btree_contains(ta, 4) == true);
    assert(btree_minimum(ta) == 3);
    assert(btree_maximum(ta) == 7);

    return 0;
}
